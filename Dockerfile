FROM isofh/tomcat8:1.0
# Take the war and copy to webapps of tomcat
RUN rm -rf /usr/local/tomcat/webapps/ROOT/*
COPY . /usr/local/tomcat/webapps/ROOT/
EXPOSE 8080
